import * as bech32 from 'bech32'
import {Hash,PKLock, Lock, Chain} from './Types'

export namespace Address {

    export function getChainType(chain: Chain) {
        switch (chain) {
            case 'local':
                return 't'
            case 'test':
                return 't'
            case 'main':
                return 'z'
        }
    }

    export function decode(chain: Chain, address: string) {
        const {prefix, words} = bech32.decode(address)

        if (prefix.length !== 2)
            throw 'invalid hrp length'

        if (prefix[0] !== getChainType(chain))
            throw 'invalid chain'

        if (words[0] !== 0)
            throw 'invalid version'

        const data = bech32.fromWords(words)

        switch (prefix[1]) {
            case 'p':
                const {hash: pkHash} = Hash.read(data, 0)
                return new PKLock(pkHash)
            default:
                throw 'not supported'
        }
    }
}

export default Address